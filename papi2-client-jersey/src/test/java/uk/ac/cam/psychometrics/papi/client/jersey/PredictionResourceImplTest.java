package uk.ac.cam.psychometrics.papi.client.jersey;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import uk.ac.cam.psychometrics.papi.client.jersey.exception.*;
import uk.ac.cam.psychometrics.papi.client.jersey.model.Prediction;
import uk.ac.cam.psychometrics.papi.client.jersey.model.PredictionResult;
import uk.ac.cam.psychometrics.papi.client.jersey.model.Trait;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PredictionResourceImplTest {

    private static final String RESOURCE_URL = "http://localhost/";
    private static final Collection<Trait> TRAITS = new HashSet<Trait>() {{
        add(Trait.BIG5);
        add(Trait.GAY);
    }};
    private static final String TOKEN = "token";
    private static final String UID = "1";
    private static final Set<String> LIKE_IDS = new HashSet<String>() {{
        add("1");
        add("2");
        add("3");
    }};

    @Mock
    private Client client;

    @Mock
    private Response response;

    @Spy
    private final PredictionResourceImpl predictionResource = new PredictionResourceImpl(client, RESOURCE_URL);

    @Before
    public void setUp() throws Exception {
        doReturn(response)
                .when(predictionResource).getByLikeIdsResponse(
                anyString(), anyCollectionOf(Trait.class), anyString(), anyString(), anySetOf(String.class));
    }

    @Test
    public void shouldReturnPersonalityWhenGetPersonalityByLikeIdsReceivedStatusOk() throws NoPredictionException {
        PredictionResult predictionResult = createPredictionResult();
        when(response.getStatus()).thenReturn(200);
        when(response.readEntity(PredictionResult.class)).thenReturn(predictionResult);
        PredictionResult returnedPredictionResult = predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
        assertEquals(predictionResult, returnedPredictionResult);
    }

    private PredictionResult createPredictionResult() {
        return new PredictionResult(UID, new HashSet<Prediction>() {{
            add(new Prediction(Trait.BIG5_OPENNESS, 1));
            add(new Prediction(Trait.BIG5_CONSCIENTIOUSNESS, 1));
            add(new Prediction(Trait.BIG5_EXTRAVERSION, 1));
            add(new Prediction(Trait.BIG5_AGREEABLENESS, 1));
            add(new Prediction(Trait.BIG5_NEUROTICISM, 1));
        }});
    }

    @Test
    public void shouldThrowExceptionWhenGetPersonalityByLikeIdsReceivedStatusUnauthorized() throws NoPredictionException {
        when(response.getStatus()).thenReturn(401);
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (TokenInvalidException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionWhenGetPersonalityByLikeIdsReceivedStatusForbidden() throws NoPredictionException {
        when(response.getStatus()).thenReturn(403);
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (TokenExpiredException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionWhenGetPersonalityByLikeIdsReceivedStatusMethodNotAllowed() throws NoPredictionException {
        when(response.getStatus()).thenReturn(405);
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (UsageLimitExceededException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionWhenGetPersonalityByLikeIdsReceivedStatusNotFound() {
        when(response.getStatus()).thenReturn(204);
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (NoPredictionException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionWhenGetPersonalityByLikeIdsReceivedUnknownStatus() throws Exception {
        when(response.getStatus()).thenReturn(666);
        try {
            predictionResource.getByLikeIds(TRAITS, TOKEN, UID, LIKE_IDS);
            fail("Was expecting exception");
        } catch (PapiClientException expected) {
        }
    }

}