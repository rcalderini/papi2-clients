package uk.ac.cam.psychometrics.papi.client.spring;

import uk.ac.cam.psychometrics.papi.client.spring.exception.NoPredictionException;
import uk.ac.cam.psychometrics.papi.client.spring.model.PredictionResult;
import uk.ac.cam.psychometrics.papi.client.spring.model.Trait;

import java.util.Collection;
import java.util.Set;

public interface PredictionResource {

    PredictionResult getByLikeIds(Collection<Trait> traits, String token, String uid, Set<String> likeIds)
            throws NoPredictionException;

}
