# Prediction API Clients #

This repository contains clients for [Prediction API](http://applymagicsauce.com/)

The subprojects are:

* `papi2-client-jersey` - Java client using Jersey
* `papi2-client-spring` - Java client using Spring Web
* `papi2-client-php` - Client for PHP 5.3+
* `papi2-thrift` - Generates Thrift libraries

## Requirements ##

* [Apache Maven 3.x](http://maven.apache.org/download.cgi)
* [Java 6.x JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) or newer
* [Apache Thrift](https://thrift.apache.org/download)
* [PHP 5.3](http://php.net/downloads.php) or newer

## How do I get set up? ##

### Java - from the Maven repository ###

You can add the following sections to your project's pom`.xml`:

    <repositories>
        <repository>
            <id>e-psychometrics-releases</id>
            <url>
                http://maven.e-psychometrics.com/release
            </url>
        </repository>
        <repository>
            <id>e-psychometrics-snapshots</id>
            <url>
                http://maven.e-psychometrics.com/snapshot
            </url>
        </repository>
    </repositories>

Then for `papi2-client-spring`:

    <dependencies>
        <dependency>
            <groupId>uk.ac.cam.psychometrics.papi</groupId>
            <artifactId>papi2-client-spring</artifactId>
            <version>2.2</version>
        </dependency>
    </dependencies>

Or for `papi2-client-jersey`:

    <dependencies>
        <dependency>
            <groupId>uk.ac.cam.psychometrics.papi</groupId>
            <artifactId>papi2-client-jersey</artifactId>
            <version>2.2</version>
        </dependency>
    </dependencies>

### Java - building it from sources ###

Go to the project directory and do `mvn clean package`

### PHP ###

You can use `papi2-client-php` straight away.

For Thrift PHP client, go to `papi2-thrift/src/thrift` and invoke the thrift compiler:

`thrift -r -out target/generated-sources/thrift -gen php:oop src/main/thrift/TPapiService.thrift`

Your generated PHP files will be in `papi2-thrift/src/thrift/target/generated-sources/thrift/php`

## Contribution guidelines ##

All improvements are welcome, especially for PHP libraries that were not well-tested.

## Who do I talk to? ##

* The owner of the repository is [Bartosz Kielczewski](http://kielczewski.eu)
* This repository is maintained by [The Psychometrics Centre of The University of Cambridge](http://www.psychometrics.cam.ac.uk/)