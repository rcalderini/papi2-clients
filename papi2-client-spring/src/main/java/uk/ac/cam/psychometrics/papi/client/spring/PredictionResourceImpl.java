package uk.ac.cam.psychometrics.papi.client.spring;

import org.springframework.http.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import uk.ac.cam.psychometrics.papi.client.spring.exception.*;
import uk.ac.cam.psychometrics.papi.client.spring.model.PredictionResult;
import uk.ac.cam.psychometrics.papi.client.spring.model.Trait;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class PredictionResourceImpl implements PredictionResource, Resource {

    private final RestTemplate restTemplate;
    private final String resourceUrl;

    public PredictionResourceImpl(final RestTemplate restTemplate, final String resourceUrl) {
        this.restTemplate = restTemplate;
        this.resourceUrl = resourceUrl;
    }

    @Override
    public PredictionResult getByLikeIds(Collection<Trait> traits, String token, String uid, Set<String> likeIds) throws NoPredictionException {
        String url = getResourceUrl(traits, uid);
        ResponseEntity<PredictionResult> response;
        try {
            response = restTemplate.postForEntity(url, createRequestEntity(token, likeIds), PredictionResult.class);
        } catch (HttpStatusCodeException e) {
            handleError(e, traits, token, uid, likeIds);
            throw new AssertionError("Execution should never reach here");
        } catch (Exception e) {
            throw new PapiClientException(String.format("Unexpected exception occurred on request to url=%s", url), e);
        }
        return handleCorrectResponse(response, traits, token, uid, likeIds);
    }

    private PredictionResult handleCorrectResponse(ResponseEntity<PredictionResult> response, Collection<Trait> traits, String token,
                                                   String uid, Set<String> likeIds) throws NoPredictionException {
        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else if (response.getStatusCode() == HttpStatus.NO_CONTENT) {
            throw new NoPredictionException(response.getStatusCode().getReasonPhrase());
        } else {
            String message = String.format("Service has returned %d status for traits=%s, token=%s, uid=%s, " +
                    "likeIds=%s, message=%s", response.getStatusCode().value(), toCsvString(traits), token, uid,
                    likeIds, response.getStatusCode().getReasonPhrase());
            throw new PapiClientException(message);
        }
    }

    private void handleError(HttpStatusCodeException e, Collection<Trait> traits, String token, String uid,
                             Set<String> likeIds) throws NoPredictionException {
        String message = String.format("Service has returned %d status for traits=%s, token=%s, uid=%s, " +
                "likeIds=%s, errorMessage=%s",
                e.getStatusCode().value(), toCsvString(traits), token, uid, likeIds, e.getResponseBodyAsString());
        if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
            throw new TokenInvalidException(message, e);
        }
        if (e.getStatusCode() == HttpStatus.FORBIDDEN) {
            throw new TokenExpiredException(message, e);
        }
        if (e.getStatusCode() == HttpStatus.METHOD_NOT_ALLOWED) {
            throw new UsageLimitExceededException(message, e);
        }
        throw new PapiClientException(message, e);
    }

    private HttpEntity<Set<String>> createRequestEntity(String token, Set<String> likeIds) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("X-Auth-Token", token);
        return new HttpEntity<>(likeIds, headers);
    }

    private String getResourceUrl(Collection<Trait> traits, String uid) {
        StringBuilder url = new StringBuilder(this.resourceUrl);
        url.append("/like_ids?");
        if (traits != null && !traits.isEmpty()) {
            url.append("&traits=").append(toCsvString(traits));
        }
        if (uid != null) {
            url.append("&uid=").append(uid);
        }
        return url.toString();
    }

    private String toCsvString(Collection<Trait> traits) {
        if (traits != null && !traits.isEmpty()) {
            StringBuilder joinedString = new StringBuilder();
            Iterator<Trait> iterator = traits.iterator();
            while (iterator.hasNext()) {
                joinedString.append(iterator.next().getName());
                if (iterator.hasNext()) {
                    joinedString.append(",");
                }
            }
            return joinedString.toString();
        } else {
            return null;
        }
    }

    @Override
    public boolean isConfigured() {
        return restTemplate != null && resourceUrl != null && !resourceUrl.isEmpty();
    }

}