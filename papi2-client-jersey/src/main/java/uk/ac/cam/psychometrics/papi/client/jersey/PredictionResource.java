package uk.ac.cam.psychometrics.papi.client.jersey;

import uk.ac.cam.psychometrics.papi.client.jersey.exception.NoPredictionException;
import uk.ac.cam.psychometrics.papi.client.jersey.model.PredictionResult;
import uk.ac.cam.psychometrics.papi.client.jersey.model.Trait;

import java.util.Collection;
import java.util.Set;

public interface PredictionResource {

    PredictionResult getByLikeIds(Collection<Trait> traits, String token, String uid, Set<String> likeIds)
            throws NoPredictionException;

}
