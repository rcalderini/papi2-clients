namespace java uk.ac.cam.psychometrics.papi.thrift.model
namespace php Papi.Thrift.Model

struct TPapiToken {
    1: required string tokenString;
    2: required i64 expires;
}

struct TPrediction {
    1: required string trait;
    2: required double value;
}

struct TPredictionResult {
    1: required string uid;
    2: list<TPrediction> predictions;
}

struct TUserLikeIds {
    1: required string uid;
    2: required set<string> likeIds;
}